package org.BenYahia.exo2;

import java.util.HashSet;
import java.util.Set;

import serie4.Marin;

public class Main {
	
	public static void main(String[] args) {
		
		 Set<Compte> comptes= new HashSet<Compte>();
		Banque banque1 = new Banque();
		
		Client client1= new Client("alain","Dupont",1231,"adresse",0762617);
		Client client2= new Client("alain","Dupont",1231,"adresse",07626175);
		Client client3= new Client("alain","Dupont",1231,"adresse",07626175);
		
		//Afficher chaque client
		System.out.println(banque1);
		
		//creer 3 comptes
		Compte c1= new Compte(Client1,1233);
		Compte c2=new Compte(Client2,1233);
		Compte c3=new Compte(Client3,1233);
		
		//Ajouter les 3 clients 
		System.out.println(c1.addClient(client1));
		System.out.println(c2.addClient(client2));
		System.out.println(c3.addClient(client3));
		
	
		//Afficher les 3 comptes
		System.out.println(banque1);
		
	}
	
}

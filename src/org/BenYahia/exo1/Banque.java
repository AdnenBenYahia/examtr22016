package org.BenYahia.exo1;

import java.util.*;




public class Banque implements Comparator<Compte>{
	
	List <Compte> comptes;
    public String nom;
    //constructeur (pour pouvoir cr�er un compte � partir d'un client et d'un solde)
	public Banque()
	{
		comptes = new ArrayList<Compte>();
	}
	
	@Override
	public int compare(Compte compte1, Compte compte2) {
		// TODO Auto-generated method stub
		
		if(compte1.client.getNom().equals(compte2.client.getNom())){
			return compte1.client.getPrenom().compareTo(compte2.client.getPrenom());
		}
		else{
			return compte1.client.getNom().compareTo(compte2.client.getNom());
		}
	}
	//question 2 b: afficher la liste de tous les comptes
	@Override
	public String toString() {
		return "Banque [comptes=" + comptes + "]";
	}
	//question 2 c: rechercher un compte � partir d'un nom
	/*public boolean recherche (String n, String p){
		
		for (i=0;i<)
		{
			
		}
	}*/
 //question 2 d: Afficher un compte � partir de son num�ro
	/*public void afficherCompte (Banque banque,int num){
		for(Compte compte : comptes){ 
		     if (banque.comptes.getId()==num){
			    System.out.println(compte);
		     }
		}
	}*/
	
	public boolean addCompte(Compte c)
	{
		return this.comptes.add(c);
	}

	/*Affiche le nombre de comptes pr�sents dans la banque*/
	public int getNombreCompte()
	{
		return this.comptes.size();	
	}

   /*Supprimer un compte de la banque*/

	public boolean removeCompte(Compte c){
		return this.comptes.remove(c);
	}
 
	
}

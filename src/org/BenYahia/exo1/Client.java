package org.BenYahia.exo1;

public class Client {
	
	public String nom;
	public String prenom;
	public int anneeNaissance;
	public String adresse;
	public int tel;
	
	// Constructeur Vide
	public Client(){
	}
	//constructeur
	public Client(int i, String n, String p, int an, String ad, int t){
		this.nom = n;
		this.prenom = p;
		this.anneeNaissance=an;
		this.adresse=ad;
		this.tel=t;
	}
	//garantir que chaque client poss�de un num�ro unique (ID): m�thode hashcode
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int ID = 1;
		ID = prime * ID + ((adresse == null) ? 0 : adresse.hashCode());
		ID = prime * ID + anneeNaissance;
		ID = prime * ID + ((nom == null) ? 0 : nom.hashCode());
		ID = prime * ID + ((prenom == null) ? 0 : prenom.hashCode());
		ID = prime * ID + tel;
		return ID;
	}
   public int ID= hashCode();//affectation du code automatique

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (anneeNaissance != other.anneeNaissance)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (tel != other.tel)
			return false;
		return true;
	}
	
	//Les m�thodes Getters et setters: qui permettent de modifier et retourner les infos d'un client
	public String getNom() {
		return nom;
	}

	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
		
	}
	public int getAnnee() {
		return anneeNaissance;
	}

	public void setAnnee(int anneeNaissance) {
		this.anneeNaissance =anneeNaissance;
	}
	
	public String getAdresse() {
		return adresse;
	}

	public void String(String adresse) {
		this.adresse =adresse;
	}
	
	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel =tel;
	}
	
	public int getId() {
		return ID ;
	}

	public void setId(int ID) {
		this.ID = ID;
	}
	@Override
	public String toString() {
		return "Client [nom=" + nom + ", prenom=" + prenom + ", anneeNaissance=" + anneeNaissance + ", adresse="
				+ adresse + ", tel=" + tel+ ", ID=" + ID + "]";
	}
	//public void id(){
		///System.out.println(ID);
	//}
	
	//les m�thodes permettant d'afficher les infos du client (toString)
	
	
	
	
    
}

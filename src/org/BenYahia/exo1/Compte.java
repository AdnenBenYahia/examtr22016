package org.BenYahia.exo1;



public class Compte /*implements crediter<Client>*/ {

	Client client = new Client();
	public int solde;
	// Constructeur Vide
	public Compte(){
	}
	//constructeur
	public Compte(Client c, int s){
		this.client=c;
		this.solde=s;
	}
	//garantir que 2 clients n'auront jamais un code identique: m�thode hashCode: affectation du code automatique
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + solde;
		return result;
	}
	public int ID=hashCode();//affectation du code automatique
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compte other = (Compte) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (solde >= 100) //il faut que le solde soit sup�rieur ou �gal � 100 euros
			return false;
		return true;
	}
	
	//Les m�thodes Getters et setters: qui permettent de modifier et retourner les infos d'un client
		public Client getClient() {
			return client;
		}

		
		public void setClient(Client client) {
			this.client = client;
		}

		public int getSold() {
			return solde ;
		}

		public void setSolde(int solde) {
			this.solde = solde;
		}
		
		public int getId() {
			return ID ;
		}

		public void setId(int ID) {
			this.ID = ID;
		}
	//Afficher toutes les infos
		@Override
		public String toString() {
			return "Compte [client=" + client + ", solde=" + solde + ", ID=" + ID + "]";
		}
		
		
		
}
